<?php

use Illuminate\Support\Facades\Session;


function flashMessage($msg='Data has been saved.', $type='success')
{
   Session::flash('flash_data',array('msg'=>$msg, 'type'=>$type));
}


function getFlashMessage(){

  $alert_type = 'alert-success';

  $flash_data = Session::get('flash_data');
  
  if($flash_data != null){

    if ($flash_data['type'] == 'success') {
       $alert_type = 'alert-success';
    }

    elseif($flash_data['type'] == 'warning'){
        $alert_type = 'alert-warning';
    }

    elseif($flash_data['type'] == 'error'){
        $alert_type = 'alert-danger';
    }

    elseif($flash_data['type'] == 'info'){
        $alert_type = 'alert-info';
    }

    echo      '<div class="alert '. $alert_type .' alert-dismissible fade show text-center" role="alert" style="opacity: 1; border-radius: 0; margin: 10px 0px;">'
                .$flash_data['msg'].
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span style="color: #fff;" aria-hidden="true">&times;</span>
              </button>
              </div>';
  }
  
  return null;
}