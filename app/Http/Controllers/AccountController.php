<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function myAccount()
    {
        $my_account = Auth::user();
        return view('account.profile', compact('my_account'));
    }

    public function updateMyAccount(Request $request)
    {
        $user = Auth::user();
        $validated_data = $request->validate([
            'name'        => 'required',
            'password'    => 'required',
        ]);

        $user->name = $validated_data['name'];
        $user->password = bcrypt($validated_data['password']);
        $user->save();

        flashMessage('Account has been updated');
        return back();
    }
}
