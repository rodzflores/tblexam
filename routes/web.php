<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function(){

    Route::get('/my-account', 'AccountController@myAccount')->name('my.account');
    Route::post('/update-account', 'AccountController@updateMyAccount')->name('update.account');

    Route::resource('/contacts', 'ContactController');
    Route::post('/contacts/search', 'ContactController@search')->name('contact.search');

});

