@extends('layouts.app')

@section('content')

    <div class="container">

        <section class="row">

            <div class="col-md-8 mx-auto">
    
                <div class="card">

                    <div class="card-header">
                      View Contact
                    </div>

                    <div class="card-body">

                        {{ getFlashMessage() }}


                       
                            
                            @method('PUT')

                            @csrf

                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" class="form-control" value="{{ $contact->first_name }}" name="first_name" id="first_name" readonly>
                            </div>

                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="form-control" value="{{ $contact->last_name }}" name="last_name" id="last_name" readonly>
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" value="{{ $contact->email }}" name="email" id="email" readonly>
                            </div>

                            <div class="form-group">
                                <label for="contact_number">Contact Number</label>
                                <input type="text" class="form-control" value="{{ $contact->contact_number }}" name="contact_number" id="contact_number" readonly>
                            </div>

                            <div class="form-group">
                                <a href="{{ route('contacts.index') }}" class="btn btn-dark">Back to List</a>
                                <input type="submit" class="btn btn-dark" data-toggle="modal" data-target="#exampleModal" value="Delete Contact">
                            </div>

                      

                    </div>

                </div>

            </div>

        </section>

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document" style="width: 500px; max-width: 500px;">
            <div class="modal-content">
                <div class="modal-header">
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body d-flex align-items-center justify-content-center" style="height: 150px;">
                    <h5 class="modal-title" id="exampleModalLabel">Contact will be deleted</h5>
                </div>
                <div class="modal-footer">
                <span class="btn btn-secondary btn-sm" data-dismiss="modal">Cancel</span>

                
                <form action="{{ route('contacts.destroy', $contact) }}" method="post">
                    @method("DELETE")
                    @csrf
                    <input class="btn btn-secondary btn-sm text-white" type="submit" value="Proceed">

                </form>
                </div>
            </div>
            </div>
        </div>

    </div>    

@endsection