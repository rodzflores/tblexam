@extends('layouts.app')

@section('content')

    <div class="container">

        <section class="row">
            
            <div class="col">
    
                <div class="card">

                    <div class="card-header">
                        Contact List
                    </div>
            
                    <div class="card-body">
            
                        <div>
                            <div class="row">
                                <div class="col">
                                    <a href="{{ route('contacts.create') }}" class="btn btn-dark mb-3">Create New Contact</a>
                                </div>
                                <div class="col text-right">
                                    
                                    <form action="{{ route('contact.search') }}" method="post">
                                        
                                        @csrf
                                       
                                       <div class="row no-gutters d-flex justify-content-end">
            
                                           <div class="col-5">
                                                <input type="text" name="search" class="form-control">
                                           </div>
            
                                           <div class="col-auto">
                                               <input type="submit" value="Search" class="btn btn-dark btn-small ml-1">
                                           </div>
                                           
                                       </div>
            
                                    </form>
                                    
                                </div>
                            </div>
                            
                            
                        </div>
            
                        <div class="table-responsive">
            
                            <table class="table">
            
                                <thead class="">
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Contact Number</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
            
                                <tbody>
                                   
                                   @foreach ($contacts as $contact)
                                    <tr>
                                        <td>{{ $contact->first_name }}</td>
                                        <td>{{ $contact->last_name }}</td>
                                        <td>{{ $contact->email }}</td>
                                        <td>{{ $contact->contact_number }}</td>
                                        <td>
                                            <a href="{{ route('contacts.edit', $contact) }}">Edit</a> |
               
                                            <a href="{{ route('contacts.show', $contact) }}">View</a>
                                        </td>
                                    </tr>
                                   @endforeach
                                    
                                </tbody>
            
                            </table>
            
                        </div>
            
                        
            
                    </div>
                </div>

                <div class="mt-2 text-center">
                    {{ $contacts->links() }}
                </div>

            </div>


           

        </section>

    </div>    

@endsection