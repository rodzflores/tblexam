@extends('layouts.app')

@section('content')

    <div class="container">

        <section class="row">

            <div class="col-md-8 mx-auto">
    
                <div class="card">

                    <div class="card-header">
                      Create New Contact
                    </div>

                    <div class="card-body">

                        {{ getFlashMessage() }}


                        <form action="{{ $contact->id !=null ? route('contacts.update', $contact) : route('contacts.store') }}" method="post">
                            
                            @if ($contact->id != null)
                                @method('PUT')
                            @endif

                            @csrf

                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input type="text" class="form-control" value="{{ $contact->first_name }}" name="first_name" id="first_name" required>
                            </div>

                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input type="text" class="form-control" value="{{ $contact->last_name }}" name="last_name" id="last_name" required>
                            </div>

                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" value="{{ $contact->email }}" name="email" id="email" required>
                            </div>

                            <div class="form-group">
                                <label for="contact_number">Contact Number</label>
                                <input type="text" class="form-control" value="{{ $contact->contact_number }}" name="contact_number" id="contact_number" required>
                            </div>

                            <div class="form-group">
                                <a href="{{ route('contacts.index') }}" class="btn btn-dark">Back to List</a>
                                <input type="submit" class="btn btn-dark" value="{{ $contact->id != null ? 'Update' : 'Create'}}">
                            </div>

                        </form>

                    </div>

                </div>

            </div>

        </section>

    </div>    

@endsection