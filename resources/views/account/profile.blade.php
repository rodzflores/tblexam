@extends('layouts.app')

@section('content')

    <div class="container">

        <section class="row">
            
            <div class="col-md-8 mx-auto">
    
                <div class="card">

                    <div class="card-header">
                      Update My Account
                    </div>

                    <div class="card-body">

                        {{ getFlashMessage() }}

                        <form action="{{ route('update.account') }}" method="post">
                            @csrf

                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name" value="{{ $my_account->name }}" required>
                            </div>

                            <div class="form-group">
                                <label for="password">New Password</label>
                                <input type="text" class="form-control" name="password" id="password">
                            </div>

                            <div class="form-group">
                                {{-- <a href="{{ route('contacts.index') }}" class="btn btn-dark">Back to List</a> --}}
                                <input type="submit" class="btn btn-dark" value="Update">
                            </div>

                        </form>                        

                    </div>
                </div>

            </div>

        </section>

    </div>    

@endsection